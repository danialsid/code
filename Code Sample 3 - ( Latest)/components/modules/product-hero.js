import React, { useEffect , useState  } from 'react'

import BlockContent from '@components/block-content'
import {
  ProductGallery,
  ProductPrice,
  ProductForm,
  ProductActions,
 
} from '@components/product'

import TTS from '../product/product-description'
import Client from 'shopify-buy';
import CartItem from '@components/cart-item';




const ProductHero = ({ product, activeVariant, onVariantChange  }) => {


  
// Initializing a client to return content in the store's primary language
const client = Client.buildClient({
  domain: 'theplugtest.myshopify.com',
  storefrontAccessToken: '874479ec80ae3f0d62509f3d1cd3aad5'
});

// Initializing a client to return translated content
const clientWithTranslatedContent = Client.buildClient({
  domain: 'theplugtest.myshopify.com',
  storefrontAccessToken: '874479ec80ae3f0d62509f3d1cd3aad5',
  language: 'ja-JP'
});


const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

    const han = product.handle;
console.log(han)

    useEffect(() => {
      client.product.fetchByHandle(han)
       
        .then(
          (result) => {
            setIsLoaded(true);
            setItems(result);
            console.log(result);
          },
          // Note: it's important to handle errors here
          // instead of a catch() block so that we don't swallow
          // exceptions from actual bugs in components.
          (error) => {
            setIsLoaded(true);
            setError(error);
            console.log(error)
          }
        )
    }, [])

    
    

 /*  let dd =  client.product.fetchByHandle(han).then((tt) => {
    const leti = tt.description ;
    const itemss = [];
  
    itemss.push(
  
     console.log(leti)
  
    )
  
    return itemss
    
  
  }) */


  


  return (
    <section className="product">
      <div className="product--content">
        <div className="product--gallery">
          <ProductGallery
            photosets={product.photos.main}
            activeVariant={activeVariant}
            hasArrows
            hasCounter
            hasThumbs
          />
        </div>

        <div className="product--details">
          <div className="product--info">
            <div className="product--header">
              <div className="product--title">
                {activeVariant && (
                  <div className="product--variant">
                    {activeVariant.title}

                    {activeVariant.lowStock && activeVariant.inStock && (
                      <span className="label is-secondary">Low Stock</span>
                    )}

                    {!activeVariant.inStock && (
                      <span className="label">Out of Stock</span>
                    )}
                  </div>
                )}
                <h1 className="product--name"> {product.productTitle}</h1>
              </div>

              <ProductPrice
                price={activeVariant?.price || product.price}
                comparePrice={
                  activeVariant?.comparePrice || product.comparePrice
                }
              />
            </div>

            {product.description && (
              <div className="product--desc">
              { <BlockContent blocks={items.description} /> } 
           

            <ul>
        {
          
      }
      </ul>
              </div>
            )}

           
<div dangerouslySetInnerHTML={{__html:  items.descriptionHtml  }} />



            <ProductForm
              product={product}
              activeVariant={activeVariant}
              onVariantChange={onVariantChange}
              className="product--form"
            />
          </div>

          <ProductActions
            activeVariant={activeVariant}
            klaviyoAccountID={product.klaviyoAccountID}
          />
        </div>
      </div>
    </section>
  )
}

export default ProductHero
