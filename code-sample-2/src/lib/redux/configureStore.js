import {createStore} from 'redux'
import Constants from '../constants';

let defaultState = {
    isLoggedIn: false,
    token: null,
    userInfo: null,
    perks: null
};

function autentication(state = defaultState, action) {
    switch (action.type) {
        case Constants.ACTION_LOGIN:
            state.isLoggedIn = true;
            state.token = action.token;
            state.userInfo = action.userInfo;
            return state;

        case Constants.ACTION_LOGOUT:
            state.isLoggedIn = false;
            state.token = null;
            state.userInfo = null;
            return state;

        case Constants.ACTION_SIGNUP:
            state.userInfo = action.userInfo;
            return state;

        // when a product is redeemed on perk details page, store the success response
        // in the store. this info will be used in perk receipt later. only one redeem product
        // info can be saved in store at one time.
        case Constants.ACTION_REDEEM_PRODUCT:
            state.redeemProduct = null;
            state.redeemProduct = action.redeemProduct;
            return state;

        case Constants.ACTION_PERKS:
            console.log('action perk called');
            state.perks = null;
            state.perks = action.perks;
            return state;

        default:
            return state
    }
}

export default function configureStore() {
    return createStore(autentication, defaultState);
}