import validator from 'validate.js';
const zipCodePattern = /\d{5}(-\d{4})?/;

const validation = {
    username: {
        presence: {
            message: '^Please enter an email address'
        },
        email: {
            message: '^Please enter a valid email address'
        }
    },
    first_name: {
        presence: {
            message: '^First name is required',
            allowEmpty: false
        }
    },
    last_name: {
        presence: {
            message: '^Last name is required',
            allowEmpty: false
        }
    },
    password: {
        presence: {
            message: '^Please enter a password',
            allowEmpty: false
        },
        length: {
            minimum: 6,
            message: '^Your password must be at least 6 characters'
        }
    },

    email: {
        presence: {
            message: '^Please enter an email address',
            allowEmpty: false
        },
        email: {
            message: '^Please enter a valid email address'
        }
    },

    BirthDate: {
        presence: {
            message: '^Please enter your date of birth',
            allowEmpty: false
        }
    },
    Zipcode: {
        format: {
            pattern: zipCodePattern,
            message: '^Zipcode must be 5 digits',
            allowEmpty: true
        }
    }
}

export default function validateValue(fieldName, value) {
    // Validate.js validates your values as an object
    // e.g. var form = {email: 'email@example.com'}
    // Line 8-9 creates an object based on the field name and field value
    let formValues = {};
    formValues[fieldName] = value;

    // Line 13-14 creates an temporary form with the validation fields
    // e.g. let formFields = {
    //                        email: {
    //                         presence: {
    //                          message: 'Email is blank'
    //                         }
    //                       }
    let formFields = {};
    formFields[fieldName] = validation[fieldName];


    // The formValues and validated against the formFields
    // the letiable result hold the error messages of the field
    const result = validator.validate(formValues, formFields);
    // If there is an error message, return it!
    if (result) {
        // Return only the field error message if there are multiple
        return result[fieldName][0];
    }

    return null;
}

export function validateIfEqual(value1, value2) {
    if (value1 === value2) {
        return null;
    }
    return "Password does not match";
}