import React, {Component} from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

import {HashRouter as Router} from "react-router-dom";
import {PrivateRoute, RedirectLoggedIn} from "./components/auth-router";

import Settings from "./screens/Settings";
import Profile from "./screens/Profile";
import MyRewards from "./screens/MyRewards";
import PerkDetails from "./screens/PerkDetails";
import SignUpForm from "./screens/SignUpForm";
import Login from "./screens/Login";
import UpgradeTier from "./screens/UpgradeTier";
import UnusedPerkReceipt from "./screens/UnusedPerkReceipt";
import PerkReceipt from "./screens/PerkReceipt";
import RedeemablePerks from "./screens/RedeemablePerks";
import Welcome from "./screens/Welcome";
import Terms from "./screens/Terms";
import Home from "./screens/Home";
import BlogTabView from "./screens/BlogTabView";
import VapingLife1 from "./screens/VapingLife1";
import VapingLife2 from "./screens/VapingLife2";
import VapingLife3 from "./screens/VapingLife3";
import VapingLife4 from "./screens/VapingLife4";
import VapingLife5 from "./screens/VapingLife5";
import VapingLife6 from "./screens/VapingLife6";
import VapingLife from "./screens/VapingLife";
import HealthDetails from "./screens/HealthDetails";
import BlogDetails from "./screens/BlogDetails";
import Notifications from "./screens/Notifications";
import Activity from "./screens/Activity";
import Navbar from "./screens/Navbar"
import Dashmenu from "./screens/Dashmenu";
import HomeScr from "./screens/HomeScr";
import Footer from "./screens/footer"




class App extends Component {

    render() {
        return (
            <div id="Appbody" className="col-md-12">

                <Router>

                    <RedirectLoggedIn exact path="/Login" component={Login}/>
                    <RedirectLoggedIn exact path="/SignUp" component={SignUpForm}/>
                    <RedirectLoggedIn exact path="/Welcome" component={Welcome}/>

                    <PrivateRoute exact path="/Dash" component={Dashmenu}/>
                    <PrivateRoute exact path="/HomeScr" component={HomeScr}/>

                    <PrivateRoute exact path="/" component={Home}/>

                    <PrivateRoute exact path="/Settings" component={Settings}/>
                    <PrivateRoute exact path="/Profile" component={Profile}/>
                    <PrivateRoute exact path="/MyRewards" component={MyRewards}/>
                    <PrivateRoute exact path="/PerkDetails/:id" component={PerkDetails}/>
                    <PrivateRoute exact path="/UpgradeTier" component={UpgradeTier}/>
                    <PrivateRoute exact path="/PerkReceipt" component={PerkReceipt}/>
                    <PrivateRoute exact path="/UnusedPerkReceipt" component={UnusedPerkReceipt}/>
                    <PrivateRoute exact path="/RedeemablePerks" component={RedeemablePerks}/>
                    <PrivateRoute exact path="/Terms" component={Terms}/>
                    <PrivateRoute exact path="/Blog" component={BlogTabView}/>
                    <PrivateRoute exact path="/VapingLife1" component={VapingLife1}/>
                    <PrivateRoute exact path="/VapingLife2" component={VapingLife2}/>
                    <PrivateRoute exact path="/VapingLife3" component={VapingLife3}/>
                    <PrivateRoute exact path="/VapingLife4" component={VapingLife4}/>
                    <PrivateRoute exact path="/VapingLife5" component={VapingLife5}/>
                    <PrivateRoute exact path="/VapingLife6" component={VapingLife6}/>
                    <PrivateRoute exact path="/VapingLife" component={VapingLife}/>
                    <PrivateRoute exact path="/HealthDetails" component={HealthDetails}/>
                    <PrivateRoute exact path="/BlogDetails/:id" component={BlogDetails}/>
                    <PrivateRoute exact path="/Notifications" component={Notifications}/>
                    <PrivateRoute exact path="/Activity" component={Activity}/>
                    <PrivateRoute exact path="/Navbar" component={Navbar}/>
                    <PrivateRoute exact path="/footer" component={Footer}/>



                </Router>

            </div>
        );
    }
}


export default App;
