import React, {Component} from 'react'
import {Linking, StyleSheet, View, Platform} from 'react-native';
import {Body, Button, Container, Toast, Header, Left, Right, Text} from 'native-base';
import Ionicons from "react-native-vector-icons/Ionicons";
import Storage from '../lib/storage';
import Spinner from "react-native-loading-spinner-overlay";
import RNFetchBlob from "rn-fetch-blob";
import Constants from "../lib/constants";
import { connect } from "react-redux";

import IconEmail from "../assets/img/icons/icon_mail.svg";

class EmailNotification extends Component{

    constructor(props) {
        super(props);
        this.state = {
            userInfo: null,
            email: '',
            isLoading: false,
        };
        this.Storage = new Storage();

    }

    async componentDidMount() {
        if (this.props.userInfo === null){
            this.props.navigation.navigate('Welcome');
        }
        this.setState({ userInfo: this.props.userInfo, email: this.props.userInfo.email });
    }

    openEmailApp = () => {

    }

    cantOpenInbox = () => {
        this.props.navigation.navigate('Login');
    }

    async sendVerificationCode(){
        let that = this;
        that.setState({isLoading: true});
        RNFetchBlob.config({
            trusty : true
        })
        .fetch("GET", Constants.API_RESEND_MAIL + this.state.userInfo.id +"/", {
            "Content-Type": "application/json"
        })
        .then(async (response) => {
            // if success, will return code 201
            that.setState({isLoading: false});
            const statusCode = response.info().status;
            if(statusCode === 201){
                Toast.show({
                    text: "Sent validation email",
                    buttonText: "OK",
                    type: "success",
                    duration: 3000
                });
            } else {
                Toast.show({
                    text: "Failed to send email",
                    buttonText: "OK",
                    type: "danger",
                    duration: 3000
                });
            }
        })
        .catch((error) => {
            Toast.show({
                text: "Failed to send email",
                buttonText: "OK",
                type: "danger",
                duration: 3000
            });
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Spinner
                    visible={this.state.isLoading}
                    textContent={'Please wait...'}
                    textStyle={styles.spinnerTextStyle}
                />
                <Header transparent>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Ionicons style={{color: '#000', fontSize: 30}} name="ios-arrow-round-back" />
                        </Button>
                    </Left>
                    <Body>
                    {/*<Title style={{color: '#000'}}>Health</Title>*/}
                    </Body>
                    <Right />
                </Header>
                <View style={{flex: 1}}>
                    <View style={styles.topView}>
                        <Text style={styles.heading}>Hey! You are almost done!</Text>
                        <IconEmail />
                        <Text style={styles.subHeading} numberOfLines={2}>Check your inbox for an email from us!</Text>
                        <Text style={{alignSelf: 'center', fontFamily: 'FiraSans-Regular', fontSize: 14}}>A verification email has been sent to:</Text>
                        <Text style={{alignSelf: 'center', fontFamily: 'FiraSans-Bold', fontSize: 14}}>{this.state.email}</Text>
                        <Text style={{alignSelf: 'center', fontFamily: 'FiraSans-Regular', fontSize: 14, marginTop: 15, textAlign: 'center'}}>Follow the link in the verification email to finish creating your account</Text>
                    </View>
                    <View style={styles.bottomView}>
                        <Button dark flat style={styles.btnCheck}
                            onPress={this.openEmailApp.bind(this)}><Text uppercase={false} style={{textAlign: 'center', width: '100%', fontFamily: 'FiraSans-Bold'}}>Let's check your inbox</Text></Button>
                        <Button light flat style={styles.btnAgain}
                                onPress={this.sendVerificationCode.bind(this)}><Text uppercase={false} style={{textAlign: 'center', width: '100%', fontFamily: 'FiraSans-Bold'}}>Send it again, I can't find it!</Text></Button>
                    </View>
                </View>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    heading: {
        alignSelf: 'center',
        fontFamily: 'FiraSans-Bold',
        fontSize: 24
    },
    subHeading: {
        fontFamily: 'FiraSans-Bold',
        fontSize: 24,
        alignSelf: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        marginBottom: 15
    },
    icon: {
        marginTop: 30,
        marginBottom: 20
    },
    topView: {
        flex: 4, justifyContent: 'center', alignItems: 'center',
        paddingLeft: 30,
        paddingRight: 30
    },
    bottomView: {
        flex: 2,
        paddingLeft: 15,
        paddingRight: 15,
        paddingTop: 30,
        paddingBottom: 15,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    btnCheck: {
        alignSelf: 'stretch',
        borderRadius: 0,
        marginBottom: 10,
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation:0
    },
    btnAgain: {
        alignSelf: 'stretch',
        borderRadius: 0,
        borderWidth: 1,
        borderColor: '#000',
        backgroundColor: 'white',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation:0
    },
    spinnerTextStyle: {
        color: '#FFF'
    }
});

const mapStateToProps = state => {
    return {
        userInfo: state.userInfo
    };
};

export default connect(mapStateToProps)(EmailNotification);