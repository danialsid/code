import React, {Component} from 'react'
import './style.css';
import {Link} from 'react-router-dom';
import bgimg from '../assets/img/vapinglife.png';

class VapingLife1 extends Component{

    render() {
        return (
            <div>
                <div className="vaping1img">
                    <img src={bgimg} alt="bgimg"/>
                </div>

                <div>
                    <div>
                        <p className="vap1title">
                            Welcome to My Vaping Life!
                        </p>
                        <p className="vapwelcombody">
                            You took the hard decision to <br/>
                            stop smoking. Congratulations!
                        </p>
                    </div>
                    <div className="vapwelbtndiv">
                    <Link to="/VapingLife2">
                        <button className="vapwelcombtn">Let's start</button>
                    </Link>
                    </div>
                </div>
            </div>
        );
    }
}


export default VapingLife1;