import React, {Component} from 'react';
import {faEye, faEyeSlash} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {Link} from 'react-router-dom';
import {Icon} from 'react-icons-kit'
import {iosArrowThinLeft} from 'react-icons-kit/ionicons/iosArrowThinLeft';
import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";
import Constants from "../lib/constants";
import {setToken, setUserInfo} from "../lib/localStorage";
import ClipLoader from 'react-spinners/ClipLoader';
import {css} from '@emotion/core';
import Navbar from './Navbar';
import LoginImg from '../assets/img/login-scr-img.png'

const override = css`
    display: block;
    margin: 0 auto;
    position: absolute;
    top:50%;
    left: 40%;
    z-index: 3;
`;

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: 'password',
            score: 'null',
            username: '',
            password: '',
            loading: false
        };
        this.showHide = this.showHide.bind(this);
        this.startloading = this.startloading.bind(this);
        this.passwordStrength = this.passwordStrength.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.loginUser = this.loginUser.bind(this);
    }

    showHide(e){
        e.preventDefault();
        e.stopPropagation();
        this.setState({
          type: this.state.type === 'input' ? 'password' : 'input'
        })  
      }
      startloading(){
        this.setState({
           loading: true
        });
      }
      passwordStrength(e){
        if(e.target.value === ''){
          this.setState({
            score: 'null'
          })
        }
      }

    handleChange(event) {
        // console.log(event.target.value);
        if (event.target.name === 'username'){
            this.setState({username: event.target.value});
        }

        if (event.target.name === 'password'){
            this.setState({password: event.target.value});
        }
    }

    async loginUser(event) {
        event.preventDefault();
        this.setState({loading: true});
        fetch( Constants.API_LOGIN, {
                method: "POST",
                body: JSON.stringify({
                        "username": this.state.username,
                        "password": this.state.password
                    }),
                headers: {
                    "Content-Type": "application/json"
                }
                
            })
            .then(response => {
                const statusCode = response.status;
                const data = response.json();
                return Promise.all([statusCode, data]);
                
            })
            .then(async ([statusCode, data]) => {
                
                if(statusCode === 400){
                    document.getElementById("Errlogin").innerHTML = "Your username or password is incorrect.";
                    this.setState({loading: false});
                    console.error(data);
                } else if(statusCode === 200){

                    let response = await fetch(Constants.API_USERS + data.user_id +"/", {
                        method: "GET",
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });
                    const info = await response.json();

                    if (!info.is_active) {
                        alert('Your account is not activated yet. Please verify your email & try again');
                        this.setState({loading: false});
                        return;
                    }
                    setToken(data.token);
                    setUserInfo(data);
                    window.location.reload(true);
                }
                
            })
            .catch((error) => {
                console.error(error);
            });

    }

    render() {
        return (
            <div >
                <Navbar />
                <div className="row" >
                     <div className="col-md-6">
                     <img className="login-banner" src={LoginImg} alt="login-banner" />
                     </div>
                    <div className="col-md-6 space">
                    <form onSubmit={this.loginUser} className="col.md.5" id="form">
                        
                        <h6 id="Errlogin" style={{color:'red',margin:'5px 0px 5px 0px'}}></h6>
                        
                            <input type="email" placeholder="Email address" name="username" className="inputfield IFdesign" onChange={this.handleChange} />
                        
                        <br/>
                        
                            <input type={this.state.type} onChange={this.handleChange} className="inputfield inuppass IFdesign"
                                autoComplete="off" placeholder="Password" name="password"/>
                            <span className="password__show" onClick={this.showHide}>{this.state.type === 'input' ?
                                <FontAwesomeIcon icon={faEye}/>
                                :
                                <FontAwesomeIcon icon={faEyeSlash}/>
                                }
                            </span>
                        
                        <br/>
                        <input type="checkbox"/><span> Remember me</span>
                        <a className="Forgot-password" href="https://goodguyvapes.net/password_reset/recover/" style={{float: 'right',}}> Forgot
                            password?</a>
                        <br/>
                      <div className="main-btn-div" >  <input className="main-btn" type="submit" value="LOG IN"/> </div>

                    </form>
                    <div className="Reg-div">
                        <p  className="reg-div-text">Don't have an account?</p>
                        <Link to="/SignUp"   className="reg-div-btn">Create An Account</Link>
                    </div>
                    </div>
            </div>

            </div>
        );
    }
}


export default Login;