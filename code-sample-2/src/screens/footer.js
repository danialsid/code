import React,{Component} from 'react';
import flogo from "../assets/img/logo-footer.png";
import twi from "../assets/img/twi.png";
import fb from "../assets/img/fb.png";
import insta from "../assets/img/insta.png";

export default class footer extends Component{
    render(){

        return(
            <div className="footer-bg">
                <div className="sec-footer-1"><img className="footer-img" src={flogo} alt="logo" /></div>
                <div className="sec-footer-2 row">
                    <div className="col-3 footer-inner"><img className="footer-img" src={twi} alt="logo" /></div>
                    <div className="col-3 footer-inner"><img className="footer-img" src={fb} alt="logo" /></div>
                    <div className="col-3 footer-inner"> <img className="footer-img" src={insta} alt="logo" /></div>
                </div>
            </div>
        );

    }
}