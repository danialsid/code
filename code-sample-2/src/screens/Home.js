import React, {Component} from 'react'
import {Link} from "react-router-dom";
import Carouselexp from '../screens/Carousel';
import Menus from '../screens/Menus';
import Constants from "../lib/constants";
import IconReward from '../assets/img/icons/icon_gift.svg';
import IconStore from '../assets/img/icons/icon_store.svg';
import IconGroup from '../assets/img/icons/icon_group.svg';
import {fetchUserInfo} from "../lib/api";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBell} from "@fortawesome/free-solid-svg-icons";
import Navbar from './navlogin';
import Homescr from './HomeScr';
import Dashmenu from './Dashmenu';
import Footer from './footer';
import History from './history';
import Homesec from './Homesec';

class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            entries: [{}, {}, {}],
            slider1ActiveSlide: 0,
            userInfo: {},
            ads: [],
        };
  
    }
  
    async componentWillMount() {
        this.fetchAds();
        fetchUserInfo().then(userInfo => {
            this.setState({userInfo});
        });
    }

    fetchAds() {
        fetch(Constants.API_ADS, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => {
            const statusCode = response.status;
            const data = response.json();
            return Promise.all([statusCode, data]);
        })
        .then(async ([statusCode, data]) => {
            
            if(statusCode === 200){
                this.setState({ads: data});
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {

        const {ads, userInfo} = this.state;

        return (
            <div className="row">
                   <Navbar />
                <div className="Main-sec-first">
                    <Dashmenu />
                    </div>
                <div className="Main-sec-second">
                <Homescr />
                <Homesec />
                <History />
                <Footer />
                    </div>    
            </div>

        );
    }
}



export default Home;