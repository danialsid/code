import React, {Component} from 'react'
import {ActivityIndicator, FlatList, Image, StatusBar, StyleSheet, View, TouchableOpacity} from 'react-native'
import {Badge, Button, Card, CardItem, Container, Content, H3, Header, Text} from 'native-base';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

import globalStyle from './styles/global';

const APIKEY = 'AIzaSyDzvDYfyYnjbsH2eRf-YnMV1HLGwLUmRJM';
// const APIKEY = 'AIzaSyCa5nGUKfWJ-DvUaWUPUyS0UMGykCureuA';

class StoreLocationMapView extends Component{

    constructor(props) {
        super(props);
        this.state = {
            stores: [{name: "Michigan Ave Store"},
                {name: "Michigan Ave Store"},
                {name: "Michigan Ave Store"}],
            loading: true
        }
    }

    render() {
        const origin = this.props.navigation.getParam('origin');
        const destination = this.props.navigation.getParam('destination');
        return (
            <Container>
                <StatusBar backgroundColor="transparent" barStyle="light-content" />
                <Header transparent style={[globalStyle.bgGrey]}>
                    <View style={{flex:1, flexDirection: 'row'}}>
                        <Button style={{position: 'absolute', left:0, zIndex: 20}}
                                transparent onPress={() => this.props.navigation.goBack()}>
                            {/*<FeatherIcon style={{color: '#000', fontSize: 30}} name="menu" />*/}
                            <Image source={require('../assets/img/icons/icon_home_black.png')}></Image>
                        </Button>
                        <Text style={{flex: 1, alignSelf: 'center', textAlign: 'center',
                            fontFamily: 'FiraSans-Bold', fontSize: 20}}>
                            Store Locations
                        </Text>
                    </View>
                </Header>
                <MapView
                    initialRegion={{
                        latitude: destination.lat,
                        longitude: destination.lng,
                        latitudeDelta: 17,
                        longitudeDelta: 17,
                    }}
                    style={styles.map}
                >
                    <Marker
                        coordinate={origin}
                    />
                    <Marker
                        coordinate={{
                            latitude: destination.lat,
                            longitude: destination.lng
                        }}
                    />
                    <MapViewDirections
                        origin={origin}
                        destination={{
                            latitude: destination.lat,
                            longitude: destination.lng
                        }}
                        strokeWidth={3}
                        strokeColor="hotpink"
                        apikey={APIKEY}
                    />
                </MapView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    map: {
        flex: 1
    }
})

export default StoreLocationMapView
