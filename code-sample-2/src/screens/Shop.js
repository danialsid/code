import React, {Component} from 'react'
import {Text, View} from 'react-native';
import {WebView} from "react-native-webview";
import {Button, Header} from "native-base";
import Ionicon from 'react-native-vector-icons/Ionicons';
import * as Progress from 'react-native-progress';
import colors from './styles/constants/colors';

class Shop extends Component{
    constructor(props) {
        super(props);
        this.state = {
            entries: [{}, {}, {}],
            slider1ActiveSlide: 0,
            userInfo: null,
            isLoading: true,
            loadProgress: 0
        }
    }

    async componentWillMount() {
        if(this.props.userInfo !== null) {
            this.setState({
                userInfo: this.props.userInfo
            });
        }
    }


    render() {
        const {loadProgress, isLoading} = this.state;

        return (
            <View style={{flex:1}}>
                <View style={{}}>
                    <Header transparent style={{paddingLeft: 15, paddingRight: 15}}>
                        <View style={{flex:1, flexDirection: 'row'}}>
                            <Button style={{position: 'absolute', left:0, zIndex: 20}}
                                    transparent onPress={() => this.props.navigation.goBack()}>
                                <Ionicon style={{color: '#000', fontSize: 30}} name="ios-arrow-round-back" />
                            </Button>
                            <Text style={{flex: 1, alignSelf: 'center', textAlign: 'center',
                                fontFamily: 'FiraSans-Bold', fontSize: 20}}>
                                Shop
                            </Text>
                        </View>
                    </Header>
                </View>
                { isLoading &&
                    <Progress.Bar
                        progress={loadProgress}
                        width={null}
                        color={colors.primary}
                        borderWidth={0}
                        height={3}
                        borderRadius={0}/>
                }
                <WebView
                    source={{ uri: "https://goodguyvapes.com/collections/all" }}
                    onLoadProgress={e => this.setState({loadProgress: e.nativeEvent.progress, isLoading: true})}
                    onLoad={() => {this.setState({isLoading: false, loadProgress: 1})}}
                    onError={(e) => {console.log('error', e.nativeEvent.progress)}}
                />
            </View>
        );
    }
}

export default Shop;