import React, {Component} from 'react'
import {Image, ImageBackground, StyleSheet, Text, View,} from 'react-native';
import {Button} from "native-base";

import IconSuccessTick from "../assets/img/icons/icon_success_tick.svg";

class SignUpSuccess extends Component{

    render() {
        return (
            <ImageBackground source={require('../assets/img/bg-green.png')}
                             style={{flex: 1}}>
                <View style={{flex: 1}}></View>
                <View style={{flex: 2.2, marginRight: 30, marginLeft: 30, backgroundColor: '#fff'}}>
                    <View style={{alignSelf: 'center', marginTop: 30, marginBottom: 30}}>
                        <IconSuccessTick style={{flex: 1}}/>
                    </View>

                    <Text style={{fontFamily: 'FiraSans-Bold', fontSize: 22, alignSelf: 'center',
                        color: '#000'}}>Congratulations!</Text>
                    <Text style={{fontFamily: 'FiraSans-Bold', fontSize: 22, alignSelf: 'center',
                        marginBottom: 20, color: '#000'}}>You are all set up!</Text>
                    <Text style={{fontFamily: 'FiraSans-Regular', fontSize: 15, alignSelf: 'center',
                        marginBottom: 20, color: '#333333', width: '80%', textAlign: 'center',
                        }}>Use your newly created login to {"\n"}
                        access your account, check your {"\n"}
                        perks, find a store near you, track {"\n"}
                        your progress and so much more!</Text>
                </View>
                <View style={styles.bottomView}>
                    <Button dark flat style={styles.btnSend}
                            onPress={() => this.props.navigation.navigate('Login')}
                            androidRippleColor={'#fff'}>
                        <Text uppercase={false} style={styles.btnTextStyle}>Let's log in to get started!</Text></Button>
                </View>
            </ImageBackground>

        );
    }
}

const styles = StyleSheet.create({
    bottomView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    btnSend: {
        alignSelf: 'stretch',
        borderRadius: 0,
        marginBottom: 15,
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation:0
    },
    btnTextStyle: {
        textAlign: 'center', width: '100%', fontFamily: 'FiraSans-Bold', color: '#fff'
    }
});

export default SignUpSuccess;