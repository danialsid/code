import React, {Component} from 'react'

import Constants from "../lib/constants";

import {fetchUserInfo} from "../lib/api";
import Logo from "../assets/img/a.png";
import reward from "../assets/img/rew.png";
import reward2 from "../assets/img/rew2.png";
import reward3 from "../assets/img/rew3.png";
import 'bootstrap/dist/css/bootstrap.min.css';


class Home extends Component{

    constructor(props) {
        super(props);
        this.state = {
            entries: [{}, {}, {}],
            slider1ActiveSlide: 0,
            userInfo: {},
            ads: [],
        };
  
    }
  
    async componentWillMount() {
        this.fetchAds();
        fetchUserInfo().then(userInfo => {
            this.setState({userInfo});
        });
    }

    fetchAds() {
        fetch(Constants.API_ADS, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => {
            const statusCode = response.status;
            const data = response.json();
            return Promise.all([statusCode, data]);
        })
        .then(async ([statusCode, data]) => {
            
            if(statusCode === 200){
                this.setState({ads: data});
            }
        })
        .catch((error) => {
            console.error(error);
        });
    }

    render() {

        const {ads, userInfo} = this.state;

        return (
            <div className="homebody">
                  
                

                <div id="homebgnew" className="row">
                    

                    <div className="hometextdiv col-5">

                       
                       <div className="row">
                           <div className="col-3">
                                <div className="profile-div ">
                                    <img className="profile-img" src={Logo} alt="logo" />
                                </div>
                           </div> 
                           <div className="col-8">
                                <h3 className="hometextnew ">Good Morning <br/> {userInfo.first_name} {userInfo.last_name}</h3>
                                <br />
                                
                           </div> 
                           <h3 className="hometextnew1"><span>CURRENT POINTS</span><br/> {userInfo.Points} </h3>
                    </div>
                    </div>

                     <div className="Secbox col-3">
                         <div className="Secbox22">
                        <h3>TIER BENEFITS</h3>
                        <div className="row">
                        <img className="rew-img col-4" src={reward} alt="logo" />
                        <img className="rew-img col-4" src={reward2} alt="logo" />
                        <img className="rew-img col-4" src={reward3} alt="logo" />
                        </div>
                        <div class="rew-text">
                            <h6>LIFE TIME DISCOUNT!</h6>
                            <p className="sub33m"><span className="m33ed">5</span> <span className="m33e">%</span> <span className="m33ef">OFF</span> ALL APPLICABLE PRODUCT TYPE </p>
                            
                        </div>
                        <p className="sub333 Active-Menu">FREE MOD UP TO $100!</p>
                        
                        </div>
                        <div className ="last-sec-secbox"> <p className="last-sec-text">{userInfo.Points} / 150000 POINTS</p></div>
                    </div>

                    <div className="Secbox1 col-3">
                    <div className="Secbox11 ">
                        
                            <div className="sec33-top">
                    <h3 className="title">CURRENT TIER STATUS</h3>
                    </div>
                    <div className="sec33-middle">
                    <img className="rew-img" src={reward} alt="logo" />
                    </div>
                    
                    </div>
                    <div className="sec33-bottom">
                    <p className="text">UPDGRADE TIER</p>
                    </div>
                    </div>
                    
                    
                   
                    
                </div> 
            </div>

        );
    }
}



export default Home;