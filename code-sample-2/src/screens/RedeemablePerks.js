import React, {Component} from 'react'
import Menus from '../screens/Menus';
import { Link } from 'react-router-dom';
import './style.css';
import Constants from "../lib/constants";
import ClipLoader from 'react-spinners/ClipLoader';
import { css } from '@emotion/core';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar from './navlogin';
import Homescr from './HomeScr';
import Dashmenu from './Dashmenu';
import Footer from './footer';

const noImg = require('../assets/img/noProductImage.jpg');


const override = css`
    display: block;
    margin: 0 auto;
    position: absolute;
    top:50%;
    left: 40%;
    z-index: 3;
`;

class RedeemablePerks extends Component{

    constructor(props) {
        super(props);
        this.state = {
            perks: [],
            loading: false
        };
        this.startloading = this.startloading.bind(this);
    }
    startloading(){
        this.setState({
           loading: true
        });
      }
    async componentWillMount() {

        fetch(Constants.API_REDEEMABLE_PRODUCTS, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }
        })
        .then(response => {
            this.setState({loading: true});
            const status = response.status;
            return Promise.all([status, response.json()]);
        })
        .then(([status, data]) => {
            if (status === 200) {
                this.setState({perks: data.results});
            }
        });
    }

    renderItems() {
        const {perks} = this.state;
        let items = [];
        if (perks.length) {
            for (const value of perks) {
                const img = value.Image ? value.Image : noImg;
                items.push(
                    <div className="col-3">
                    <div className="redeemproduct " key={value.id}>
                        <Link to={"/PerkDetails/" + value.id}>
                            <img src={img} alt="Product" />
                            <p className="redprotitle">{value.Title}</p> <span className="redprodetails">Details</span>
                            <p className="redpropint">{value.RedeemPoints} points</p>
                        </Link>
                    </div>
                    </div>
                )
            }
        }

        return items;
    }

    render() {

        return (
            <div>
                      <div className="row">
                   <Navbar />
                <div className="Main-sec-first">
                    <Dashmenu />
                    </div>
                <div className="Main-sec-second">
                <Homescr />

                <div className="redeemeableblock">
                 
                    <h2 className="Redeemableheader">Redeemable Perks</h2>
                </div>
                <div className="row align-center">
                { this.renderItems() }
                </div>
                <Footer />
                    </div>    
                    
            </div>


               
                
           </div>
        );
    }
}


export default RedeemablePerks;