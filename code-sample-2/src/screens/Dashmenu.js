import React,{Component} from 'react';
import './style.css';
import img1 from "../assets/img/dash-without-color.png";
import img2 from "../assets/img/rewards.png";
import img3 from "../assets/img/history.png";
import img4 from "../assets/img/gear.png";
import { Link } from 'react-router-dom';

export default class menu extends Component{
    render(){

        return(
            <div className="dash-bg">
                <div className="dash-div"><Link to="/"><img className="dash-img" src={img1} alt="logo" /></Link></div>
                <div className="dash-div"><Link to="/RedeemablePerks"><img className="dash-img" src={img2} alt="logo" /></Link></div>
                <div className="dash-div"><Link to="/Activity"><img className="dash-img" src={img3} alt="logo" /></Link></div>
                <div className="dash-div"><img className="dash-img" src={img4} alt="logo" /></div>
            </div>
        );

    }
}