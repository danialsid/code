import React, {Component} from 'react'
import {StyleSheet, View} from 'react-native';
import {Button, Container, Form, Header, Input, Item, Left, Right, Text, Title, Content} from 'native-base';
import Ionicons from "react-native-vector-icons/Ionicons";

class ForgotPassword extends Component{

    render() {
        return (
            <Container style={styles.container}>
                <Header transparent>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Ionicons style={{color: '#000', fontSize: 30}} name="ios-arrow-round-back" />
                        </Button>
                    </Left>
                    {/*<Body>
                    <Title style={{color: '#000', fontFamily: 'FiraSans-Bold'}}>TT</Title>
                    </Body>*/}
                    <Right></Right>
                </Header>
                <Content contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={styles.topView}>
                        <Text style={styles.heading}>Can't remember your password?</Text>
                        <Text style={styles.subHeading}>
                            We've got your back! just enter your email address below and we'll help you set up a brand
                            new password!
                        </Text>
                        <Text style={styles.label}>Email</Text>
                        <Form style={styles.form}>
                            <Item regular>
                                <Input placeholder="gracesarawsati@gmail.com" style={styles.input} />
                            </Item>
                        </Form>
                    </View>
                    <View style={styles.bottomView}>
                        <Button dark flat style={styles.btnSend} onPress={() => alert('Button Pressed!')}>
                            <Text uppercase={false} style={styles.btnTextStyle}>Send it</Text></Button>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center'
    },
    heading: {
        fontFamily: 'FiraSans-Bold',
        fontSize: 24
    },
    subHeading: {
        alignSelf: 'flex-start', fontFamily: 'FiraSans-Regular', fontSize: 14,
        color: '#363636',
        marginTop: 15,
        marginBottom: 15
    },
    form: {
        // alignItems: 'stretch',
        marginBottom: 15
    },
    label: {
        color: '#9193A2', fontFamily: 'FiraSans-Bold', fontSize: 14, marginTop: 15,
        marginBottom: 10
    },
    input: {
        borderColor: '#DEDEDE',
        paddingLeft: 15,
        height: 40,
        fontFamily: 'FiraSans-Regular',
        color: '#000',
        fontSize: 14
    },
    icon: {
        marginTop: 30,
        marginBottom: 20
    },
    topView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        /*justifyContent: 'flex-start',
        alignItems: 'stretch',*/
        // backgroundColor: '#0f0'
    },
    bottomView: {
        flex: 1,
        paddingLeft: 20,
        paddingRight: 20,
        // paddingTop: 30,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        // backgroundColor: '#ed0',

    },
    btnSend: {
        alignSelf: 'stretch',
        borderRadius: 0,
        marginBottom: 15,
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation:0
    },
    btnTextStyle: {
        textAlign: 'center', width: '100%', fontFamily: 'FiraSans-Bold'
    }
});

export default ForgotPassword;