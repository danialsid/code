import React, {Component} from 'react'
import {Image, StyleSheet, View} from 'react-native';
import {Button, Text} from 'native-base';
import Ionicons from "react-native-vector-icons/Ionicons";
import Carousel from "react-native-snap-carousel";

let activeSlideIndex = 0;
class HorizontalScrollBar extends Component{
    state = {
        data: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],
        activeSlideIndex: 3
    }

    componentDidMount() {
        activeSlideIndex = this.props.initalActiveIndex
    }

    _renderItem ({item, index}) {
        return (
            <View style={styles.numberContainer}>
                <Text style={[styles.number, {color: index === activeSlideIndex ? 'green' : '#333'}]}>{item}</Text>
            </View>

        );
    }

    _onSnapItem = slideIndex => {
        activeSlideIndex = slideIndex;
        this.props.onChange(slideIndex + 1);
        this.setState({
            activeSlideIndex: slideIndex
        })
    }

    render() {
        return (
            <View style={styles.bottomTop}>
                <View style={styles.mask} />
                <View style={{backgroundColor: 'transparent', marginTop: -50}}>
                    <Carousel
                        ref={(c) => { this._carousel = c; }}
                        data={this.state.data}
                        renderItem={this._renderItem}
                        sliderWidth={250}
                        itemWidth={50}
                        firstItem={3}
                        onSnapToItem={(slideIndex) => this._onSnapItem(slideIndex)}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    bottomTop: {
        flex:1,
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: 'transparent'
    },
    numberContainer: {
        width: 50,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        zIndex: 10,
        backgroundColor: 'transparent'
    },
    number: {
        fontSize: 25
    },
    mask: {
        zIndex: 83,
        width: 50,
        height: 50,
        borderLeftWidth: 2,
        borderRightWidth: 2,
        borderColor: '#7BC81A',
        backgroundColor: 'transparent',
    }
});

export default HorizontalScrollBar
