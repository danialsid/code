import React, {Component} from 'react'
import Constants from "../lib/constants";
import {fetchUserInfo} from "../lib/api";
import 'bootstrap/dist/css/bootstrap.min.css';

import banner from "../assets/img/banner.png";
import bot from "../assets/img/bot.png";
import honey from "../assets/img/honey.png";
import bot3 from "../assets/img/3bot.png";
import sec1 from "../assets/img/sec1.png";
import sec2 from "../assets/img/sec2.png";
import sec3 from "../assets/img/sec3.png";
import { Link } from 'react-router-dom';


export default class homesec extends Component{
    render(){

        return(
            <div className="home-sec">
                <div className="banner-div"><img className="banner-img" src={banner} alt="logo" /></div>

                <div className="row home-sec-2">
                    <div className="col-6 home-sec-2-1">
                        <h2 className="Active-Menu homesec-heading">Redeem Rewards</h2>
                        <h2 className="Active-Menu homesec-heading">In-Store Today!</h2>
                        <p className="text">Visit your neastest Good Guy Vapes location to redeem.A sales associate will assist you and properly apply your perk or discount to your next purchase</p>
                    </div>
                    <div className="col-6 home-sec-2-2">
                        <div className="sub-home-sec-2-2 row">
                        <h2 className="home-sec-2-2-heading col-12">STAFF PICKS</h2>
                        <div className="sub-sub col-6">
                        <img className="homesec-img" src={honey} alt="logo" />
                        <p className="Active-Menu">CBD Honey Stick</p>
                        </div>
                        <div className="sub-sub col-6">
                        <img className="homesec-img" src={bot} alt="logo" />
                        <p className="Active-Menu"> CBD E-Juice 1500MG</p>
                        </div>
                        <div className="sub-sub col-6">
                        <img className="homesec-img" src={bot3} alt="logo" />
                        <p className="Active-Menu">CBD Tincture 1500MG</p>
                        </div>
                    </div>
                    </div>
                </div>

                <div className="row home-sec-3"> 
                <div className="col-4">
                    <div className="sub3-home-3">
                    <Link to="/RedeemablePerks">   <img className="homesec3-img" src={sec1} alt="logo" />
                    <p className="text-home-3">CBD REWARDS</p> </Link>
                    </div>
                </div>
                <div className="col-4">
                <div className="sub3-home-3">
                <Link to="/RedeemablePerks">  <img className="homesec3-img" src={sec1} alt="logo" />
                    <p className="text-home-3">MERCH REWARDS</p> </Link>
                    </div>
                </div>
                <div className="col-4">
                <div className="sub3-home-3">
                <Link to="/RedeemablePerks">   <img className="homesec3-img" src={sec3} alt="logo" />
                    <p className="text-home-3">DELTA-8  REWARDS</p> </Link>
                    </div>
                </div>
                <div className="col-12" > <p className="Active-Menu sub3"><Link to="/RedeemablePerks"> VIEW ALL </Link></p></div>
                
                </div>
                
            </div>
        );

    }
}