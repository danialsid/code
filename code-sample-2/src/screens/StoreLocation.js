import React, {Component} from 'react'
import {ActivityIndicator, FlatList, Image, StatusBar, StyleSheet, View, TouchableOpacity, Alert} from 'react-native'
import {Badge, Button, Card, CardItem, Container, Content, H3, Header, Text} from 'native-base';
import Ionicons from "react-native-vector-icons/Ionicons";
import RNFetchBlob from "rn-fetch-blob";
import openMap from 'react-native-open-maps';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {Call} from 'react-native-openanything';

import globalStyle from './styles/global';
import Constants from "../lib/constants";
import Storage from "../lib/storage";

let noImg = require('../assets/img/noProductImage.jpg');
const GEOCODEURL = 'https://maps.googleapis.com/maps/api/geocode/json';
const APIKEY = 'AIzaSyDzvDYfyYnjbsH2eRf-YnMV1HLGwLUmRJM';

class StoreLocation extends Component{
    constructor(props) {
        super(props);
        this.state = {
            stores: [],
            loading: true,
            origin: {}
        }
        this.Storage = new Storage();
    }

    async componentDidMount() {
        let token = await this.Storage.find("currentPosition");
        token = JSON.parse(token);
        const url = `https://goodguyvapes.net/api/nearest_stores/${token.latitude}/${token.longitude}/`;
        RNFetchBlob.config({
            trusty : true
        })
        .fetch("GET", url, {
            "Content-Type": "application/json"
        }).then((res) => {
            return res.json()
        })
            .then((data) => {
              this.setState({
                  stores: data,
                  loading: false,
                  origin: {
                      latitude: token.latitude,
                      longitude: token.longitude,
                  }
              });
            })
            .catch((err) => {
                console.log(err)
                return null
            })
    }

    _keyExtractor = (item, index) => index.toString();

    gotoStoreLocationMapView = (item) => {
        const latlng = this.state.origin
        const end = item.Address2
            ? `${item.Address1} ${item.Address2}, ${item.City}, ${item.State}`
            : `${item.Address1}, ${item.City}, ${item.State}`;
        const url = `${GEOCODEURL}?latlng=${latlng.latitude},${latlng.longitude}&key=${APIKEY}`
        RNFetchBlob.config({
            trusty : true
        })
        .fetch("GET", url, {
            "Content-Type": "application/json"
        }).then((res) => {
            openMap({
              latitude: latlng.latitude,
              longitude: latlng.longitude,
              start: res.json().plus_code.compound_code,
              end: end,
              provider: 'google'
            });
            return;
        })
    }

    call = item => {
      Call(item.PhoneNumber).catch(err => {});
    }

    render() {
        const stores = this.state.stores;
        return (
            <Container>
                <StatusBar backgroundColor="transparent" barStyle="light-content" />
                <Header transparent style={[globalStyle.bgGrey]}>
                    <View style={{flex:1, flexDirection: 'row'}}>
                        <Button
                            style={{position: 'absolute', left:0, zIndex: 20}}
                            transparent onPress={() => this.props.navigation.goBack()}
                        >
                            <Ionicons style={{color: '#000', fontSize: 22}} name="ios-arrow-round-back" />
                        </Button>
                        <Text
                            style={{flex: 1, alignSelf: 'center', textAlign: 'center',
                            fontFamily: 'FiraSans-Bold', fontSize: 20}}
                        >
                            Store Locations
                        </Text>
                    </View>
                </Header>
                <Content style={[styles.content, globalStyle.bgGrey]}>
                    {this.state.loading && (
                        <ActivityIndicator size="large" />
                    )}
                    <FlatList
                        data={this.state.stores}
                        keyExtractor={this._keyExtractor}
                        renderItem={({item}) => {
                            let img = item.Image === null ? noImg : {uri: item.Image};

                            return (
                                <View style={styles.card}>
                                    <View style={{flex:1, flexDirection: 'row', marginBottom: 8}}>
                                        <Text style={{fontFamily: 'FiraSans-Bold', fontSize: 14}}>{item.StoreName}</Text>
                                        <Text
                                            style={{
                                                fontFamily: 'FiraSans-Bold',
                                                fontSize: 12,
                                                color: '#999AAB',
                                                textAlignVertical: 'center',
                                                marginLeft: 10
                                            }}
                                        >
                                            {item.Distance} away
                                        </Text>
                                    </View>
                                    <View style={{flex:3, marginBottom: 10}}>
                                        <Text style={{width: '100%', fontFamily: 'FiraSans-Regular', fontSize: 14}}>
                                            {item.Address1} {item.Address2}
                                        </Text>
                                        <Text style={{width: '100%', fontFamily: 'FiraSans-Regular', fontSize: 14}}>
                                            {item.City} {item.State}, {item.Zipcode}
                                        </Text>
                                        <Text style={{width: '50%', fontFamily: 'FiraSans-Regular', fontSize: 14}}>
                                            {item.PhoneNumber}
                                        </Text>
                                        <Text
                                            style={{
                                                fontFamily: 'FiraSans-Regular',
                                                fontSize: 14,
                                                textAlignVertical: 'center',
                                                marginTop: 10
                                            }}
                                        >
                                            Driving time: {item.Duration}
                                        </Text>
                                    </View>
                                    <View style={{flex:1, flexDirection:'row', justifyContent: 'space-between'}}>
                                        <Badge success style={styles.newBadge}>
                                            <TouchableOpacity onPress={() => this.call(item)}>
                                                <Text style={{fontFamily: 'FiraSans-Bold', fontSize: 13, textAlignVertical: 'center'}}>Call</Text>
                                            </TouchableOpacity>
                                        </Badge>
                                        <Badge success style={styles.newBadge}>
                                            <TouchableOpacity onPress={() => this.gotoStoreLocationMapView(item)}>
                                                <Text style={{fontFamily: 'FiraSans-Bold', fontSize: 13, textAlignVertical: 'center'}}>Directions</Text>
                                            </TouchableOpacity>
                                        </Badge>
                                    </View>
                                </View>
                            );
                        }}
                    />
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#fafafa'
    },
    headerText: {
        color: '#000'
    },
    colorBlack: {
        color: '#000'
    },
    colorGrey: {
        color: '#333'
    },
    mb10: {
        marginBottom: 25
    },
    cardText: {
        flexWrap: 'wrap',
        justifyContent: 'flex-start',
        flexDirection:'column',
        flex: 1
    },
    defaultBg: {
        backgroundColor: '#f9f9f9'
    },
    content: {
        paddingLeft: 20,
        paddingRight: 20,
        paddingTop: 20
    },
    card: {
        backgroundColor: '#fff',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 15,
        paddingRight: 15,
        // position: 'relative',
        // minWidth:250,
        // minHeight: 250,
        // marginLeft: 15,
        // marginRight: 15,
        marginBottom: 25,
        shadowColor: '#000000',
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowRadius: 5,
        shadowOpacity: 0.5,
        // elevation: 5,
        // borderRadius: 0
    },
    newBadge: {
        backgroundColor: '#7ed321',
        borderRadius: 0,
        width: '40%'
    }
})

export default StoreLocation;
