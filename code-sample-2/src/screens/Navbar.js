import React,{Component} from 'react';
import './style.css';
import Logo from '../assets/img/logo.png'
import AuthService from '../lib/auth-service';

export default  class Navbar extends Component{




    render() {

       

        return (
            <div className="container Bg-Color">
                <div className="logo"><img className="logo-img" src={Logo} alt="logo" /></div>
                
                <nav >
                
                <li><a href="https://goodguyvapes.com/"> HOME  </a></li>
                <li><a href="https://goodguyvapes.com/pages/search-results-page?collection=all"> ALL PRODUCT</a></li>
                <li><a href="https://goodguyvapes.com/pages/search-results-page?collection=all-e-juice"> SHOP E-LIQUID</a></li>
                <li><a href="https://goodguyvapes.com/pages/search-results-page?collection=shop-cbd&page=1&rb_product_type=CBD+Accessory%7CCBD+Capsules%7CCBD+E-Juice%7CCBD+Edibles%7CCBD+Flower%7CCBD+Pre-Roll%7CCBD+Tincture%7CCBD+Topicals"> SHOP CBD</a></li>
                <li><a href="https://goodguyvapes.com/pages/search-results-page?collection=hookah&page=4&rb_product_type=Hookah%7CHookah+Accessories%7CHookah+Ash+Catchers%7CHookah+Bowls%7CHookah+Hoses"> SHOP HOOKAH</a></li>
                <li><a href="https://goodguyvapes.com/pages/search-results-page?collection=smoke"> SHOP SMOKE</a></li>
                <li><a href="https://goodguyvapes.com/apps/store-locator"> OUR LOCATIONS</a></li>
                <li><a href="https://goodguyvapes.com/pages/our-mission"> ABOUT US</a></li>
                <li className="Active-Menu"> REWARDS</li>
                </nav >

            </div>
            
        );
}

}