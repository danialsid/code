import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import AuthService from '../lib/auth-service';

const PrivateRoute = ({ component: Component, ...rest }) => {
    
    let isLoggedIn = AuthService.isLoggedIn();

    // console.log({isLoggedIn});

    return (
        <Route {...rest} render={
            props => isLoggedIn ? 
            ( <Component {...props} /> ) : 
            ( <Redirect to={{ pathname: "/Login", state: { from: props.location } }
        } /> )} />
    )
};

const RedirectLoggedIn = ({ component: Component, ...rest }) => {
    
    let isLoggedIn = AuthService.isLoggedIn();

    return (
        <Route {...rest} render={
            props => !isLoggedIn ? 
            ( <Component {...props} /> ) : 
            ( <Redirect to={{ pathname: "/", state: { from: props.location } }
        } /> )} />
    )
};

export { PrivateRoute, RedirectLoggedIn };